#!/usr/bin/env python
import numpy as np
import cv2
import Ros
import lis
import rospy
X = 640
Y = 480
vid  = lis.image_converter()

img = np.zeros((300,512,3),np.uint8)
cv2.namedWindow('image')
import math
img = np.ones((640,480,3), np.uint8)
img2 = np.ones((640,480,3), np.uint8)
a = False

i = 0
xi,yi = -1,-1

upper_blue= np.array([100,100,100])
lower_blue = np.array([255,255,255])
    
    
def match():

        contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)     
        
        areas = []
        max_area = []
        max_index_area = 0
        test = 0

        for t in contours:
                test = cv2.contourArea(t)
        

        if test != 0 :
                for c in contours:
               
                        ar = cv2.contourArea(c)
                        areas.append(ar)                        

                max_area = max(areas)
                max_area_index = areas.index(max_area)

                cnt = contours[max_area_index]
        
                
                (b,c), radius = cv2.minEnclosingCircle(cnt)
                center = (int(b),int(c))
                X = int(b)
                radius = int(radius)
                cv2.circle(image,center,radius,(0,255,0),2)



def region(event,x,y,flags,param):
    global i,xi,yi,lower_blue,upper_blue
    maxh = 255
    minh = 0
    maxs = 255
    mins = 0
    maxv = 0
    minv = 255
    if event == cv2.EVENT_LBUTTONDOWN:
        xi = x
        yi = y
        i = 1
               
            
    
    if i == 1:
        if event == cv2.EVENT_LBUTTONUP:
            i = 0
                    
            cv2.rectangle(img,(xi,yi),(x,y),(255,0,0),1)
            temp = img[yi+2:y,xi+2:x]
            
                        
            t = temp
            
            o = t.shape[0]
            u = t.shape[1]
            
            
            p = t[o-1,u-1]
           
            k = 0
            j = 0
            
            for k in xrange(o):
                for j in xrange(u):
                    m = t[k-3,j-3]
                    n = t[k-3,j-3]
                    hl = m[0]
                    hh = n[0]
                                                    
                    if maxh > hh:
                        maxh  =int(hh)
                    if minh < hl:
                        minh = int(hl)
            
            k = 0
            j = 0
            for k in xrange(o):
                for j in xrange(u):
                    m = t[k-3,j-3]
                    n = t[k-3,j-3]
                    sl = m[1]
                    sh = n[1]
                                                    
                    if maxs > sh:
                        maxs  = int(sh)
                    if mins < sl:
                        mins = int(sl)
            
            k = 0
            j = 0
            
            for k in xrange(o):
                for j in xrange(u):
                    m = t[k-3,j-3]
                    n = t[k-3,j-3]
                    vl = m[2]
                    vh = n[2]
                                                    
                    if maxv > vh:
                        maxv  = int(vh)
                    if minh < vl:
                        minh = int(vl)
           
            upper_blue= np.array([minh,mins,minv])
            lower_blue = np.array([maxh,maxs,maxv])
                      
rospy.init_node('image_converter', anonymous=True)

while(True): 
    # Our operations on the frame come here  
    
    # Display the resulting frame
    
    image = vid.read() 

   
           
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        
        
        
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    
    mask = cv2.inRange(hsv, lower_blue, upper_blue)
    median = cv2.medianBlur(mask,7)
       
    ret2, thresh = cv2.threshold(median,0,255,cv2.THRESH_BINARY)
                 
   
               
            
    match()
 
      
    
    cv2.imshow('image2',image)
    cv2.imshow('mask', mask)
   
    if a == True :
	cv2.imshow('image',img)
        
      
    
    
    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
        break
    
    if key == ord('c'):
        img = image
        img = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
        a = True 
        cv2.setMouseCallback('image',region)             
        cv2.waitKey(1)
    if key == ord('e'):
        a = False
        cv2.destroyWindow('image')
        cv2.destroyWindow('tmp')
        cv2.waitKey(10)
        
        
      
# When everything done, release the capture
vid.release()
cv2.destroyAllWindows()
