#!/usr/bin/env python
import roslib
import rospy
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import sys


class image_converter:

	
  img = np.ones((640,480,3), np.uint8)
  def __init__(self):
    

    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("topiccv",Image,self.callback)

  def callback(self,data):
    try:
      self.img = self.bridge.imgmsg_to_cv2(data, "bgr8")
    except CvBridgeError as e:
      print(e)
	
   
  def read(self):
    return self.img
    



