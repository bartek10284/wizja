#!/usr/bin/env python
import roslib
import numpy as np
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from geometry_msgs.msg import Pose2D

from cv_bridge import CvBridge, CvBridgeError
from threading import Thread
import time
from Queue import Queue
import math

from std_msgs.msg import String
global pose
pose = Pose2D()


img = np.zeros((300,512,3),np.uint8)
cv2.namedWindow('image')
import math
img = np.ones((640,480,3), np.uint8)

a = False

i = 0
xi,yi = -1,-1

upper_blue= np.array([100,100,100])
lower_blue = np.array([255,255,255])

bridge = CvBridge()
image = np.zeros((480,640,3), np.uint8)	
img = np.zeros((300,512,3),np.uint8)
X = 480
Y = 480        
global pub

#---------------------------
def initialize():
	global pub	
	rospy.init_node('image_converter', anonymous=True)    
        image_sub = rospy.Subscriber("topiccv",Image,callback)
	pub = rospy.Publisher('Mpose', Pose2D, queue_size=10)
    
#-------------------------------------------

def region(event,x,y,flags,param):
    global i,xi,yi,lower_blue,upper_blue
    maxh = 255
    minh = 0
    maxs = 255
    mins = 0
    maxv = 0
    minv = 255
    if event == cv2.EVENT_LBUTTONDOWN:
        xi = x
        yi = y
        i = 1
               
            
    
    if i == 1:
        if event == cv2.EVENT_LBUTTONUP:
            i = 0
                    
            cv2.rectangle(img,(xi,yi),(x,y),(255,0,0),1)
            temp = img[yi+2:y,xi+2:x]
            
                        
            t = temp
            
            o = t.shape[0]
            u = t.shape[1]
            
            
            p = t[o-1,u-1]
           
            k = 0
            j = 0
            
            for k in xrange(o):
                for j in xrange(u):
                    m = t[k-3,j-3]
                    n = t[k-3,j-3]
                    hl = m[0]
                    hh = n[0]
                                                    
                    if maxh > hh:
                        maxh  =int(hh)
                    if minh < hl:
                        minh = int(hl)
            
            k = 0
            j = 0
            for k in xrange(o):
                for j in xrange(u):
                    m = t[k-3,j-3]
                    n = t[k-3,j-3]
                    sl = m[1]
                    sh = n[1]
                                                    
                    if maxs > sh:
                        maxs  = int(sh)
                    if mins < sl:
                        mins = int(sl)
            
            k = 0
            j = 0
            
            for k in xrange(o):
                for j in xrange(u):
                    m = t[k-3,j-3]
                    n = t[k-3,j-3]
                    vl = m[2]
                    vh = n[2]
                                                    
                    if maxv > vh:
                        maxv  = int(vh)
                    if minh < vl:
                        minh = int(vl)
           
            upper_blue= np.array([minh,mins,minv])
            lower_blue = np.array([maxh,maxs,maxv])
                      
#---------------------------------------------------------


def callback(data):
	global a, lower_blue, upper_blue
		
	try:
		cv_image = bridge.imgmsg_to_cv2(data, "bgr8")
		image = cv_image
		
               

		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        
        
        
   		hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    
    		mask = cv2.inRange(hsv, lower_blue, upper_blue)
    		median = cv2.medianBlur(mask,7)
       
    		ret2, thresh = cv2.threshold(median,0,255,cv2.THRESH_BINARY)
				
	       
		

	  	contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)     
		
		areas = []
		max_area = []
		max_index_area = 0
		test = 0

		for t in contours:
		        test = cv2.contourArea(t)
		

		if test != 0 :
			for c in contours:
			       
				ar = cv2.contourArea(c)
				areas.append(ar)                        

				max_area = max(areas)
				max_area_index = areas.index(max_area)

				cnt = contours[max_area_index]
		
				
			(x,y), radius = cv2.minEnclosingCircle(cnt)
			center = (int(x),int(y))
			radius = int(radius)
			cv2.circle(hsv,center,radius,(0,255,0),2)
			

			cv2.line(hsv,(X/2,Y/2),(int(x),int(y)),(255,0,0),2,4)
        
       			ax = (x-(X/2))/(X/2)
        		ay = (y-(Y/2))/(Y/2)
        		c = math.sqrt(pow(ax,2)+pow(ay,2))
        		q = math.sin(ax/c)
        		dane = 'osX = '+str(ax)+'osY= '+str(ay)+"kat= "+str(q)
        
       			font = cv2.FONT_HERSHEY_SIMPLEX
        		cv2.putText(hsv, dane ,(50,15),font, 0.5,(255,255,255),2)
			pose.x = ax
			pose.y = ay
			pose.theta = q	
			pub.publish(pose)
						
			
		cv2.imshow('lol',image)
		cv2.imshow('mask2',mask)		             
	
		k = cv2.waitKey(1) & 0xff
		if k ==27:
			cv2.destroyAllWindows()		
			cv2.waitKey(10)
			cv2.destroyAllWindows()


		if a == True :   
    
    			key = cv2.waitKey(1) & 0xFF
    		
    
    			if key == ord('c'):
        			img = image
        			img = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)
        			a = True 
       	 			cv2.setMouseCallback('image',region)             
        			cv2.waitKey(1)
    			if key == ord('e'):
        			a = False
        			cv2.destroyWindow('image')
        			cv2.destroyWindow('tmp')
        			cv2.waitKey(10)
			
				cv2.destroyAllWindows()
            
        except CvBridgeError as e:
          	print(e)	



def talker():
	initialize()
    	while():
		k = cv2.waitKey(1) & 0xff
		if k ==27:
			cv2.destroyAllWindows()		
			cv2.waitKey(10)
			cv2.destroyAllWindows()
		
if __name__=='__main__':
	talker()
 

