#!/usr/bin/env python
'''
Created on Sep 8, 2016

@author: euroavionic
'''
import cv2
import numpy as np


class shift():    
    
    pos =(0,0,10,10) 
    one = True
   
    
    def __init__(self, bgr,pos):
        
        
        self.hsv_roi =  cv2.cvtColor(bgr, cv2.COLOR_BGR2HSV)
        self.mask = cv2.inRange(self.hsv_roi, np.array((0., 60.,32.)), np.array((180.,255.,255.)))
        self.roi_hist = cv2.calcHist([self.hsv_roi],[0],self.mask,[180],[0,180])
        cv2.normalize(self.roi_hist,self.roi_hist,0,255,cv2.NORM_MINMAX)
        self.term_crit = ( cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 1 )
        self.pos = pos
     
    def init(self,pos,bgr):     
        
        self.pos = pos
        bgr = cv2.imread('temp.jpg')
        self.hsv_roi =  cv2.cvtColor(bgr, cv2.COLOR_BGR2HSV)
        self.mask = cv2.inRange(self.hsv_roi, np.array((0., 60.,32.)), np.array((180.,255.,255.)))
        self.roi_hist = cv2.calcHist([self.hsv_roi],[0],self.mask,[180],[0,180])
        cv2.normalize(self.roi_hist,self.roi_hist,0,255,cv2.NORM_MINMAX)       
        
        
    
    def start(self,image,frame):
                
        self.hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        self.dst = cv2.calcBackProject([self.hsv],[0],self.roi_hist,[0,180],1)
        
        ret, self.pos = cv2.meanShift(self.dst, self.pos, self.term_crit)
        x,y,w,h = self.pos
        cv2.rectangle(frame, (x,y), (x+w,y+h), 255,2)
    
    
           
        
