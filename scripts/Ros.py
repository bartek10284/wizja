#!/usr/bin/env python
import roslib
import rospy
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np


class message():
	
	bridge = CvBridge()
	image = np.ones((640,480,3), np.uint8)
	

	def __init__(self):
		self.image_pub = rospy.Publisher("topiccv",Image,queue_size = 10)
		rospy.init_node('viev',anonymous = True)
		
	
	def send(self,frame):
		image_message = self.bridge.cv2_to_imgmsg(frame,"bgr8")
		self.image_pub.publish(image_message)
		
	
		
		 
	
